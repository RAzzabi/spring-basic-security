# Spring Security : Basic Authentication and Authorization using spring boot

## What is Basic Authentication
Basic authentification is a standard HTTP header with the user and password encoded in base64 : __Authorization: Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==__.The __userName__ and __password__ is encoded in the format __username:password__. This is one of the simplest technique to protect the REST resources because it does not require cookies. session identifiers or any login pages.

In case of basic authentication, the username and password is only encoded with Base64, but not encrypted or hashed in any way. Hence, it can be compromised by any man in the middle. Hence, it is always recommended to authenticate rest API calls by this header over a ssl connection.

## BasicAuthenticationFilter in Spring
BasicAuthenticationFilter in Spring is the class which is responsible for processing basic authentication credentials presented in HTTP Headers and putting the result into the SecurityContextHolder. The standard governing HTTP Basic Authentication is defined by RFC 1945, Section 11, and BasicAuthenticationFilter confirms with this RFC.

## Step 1 : Init a Spring boot project with spring starter
* 1- Open https://start.spring.io/
* 2- Configure a new project as follow :
```
  Group : com.miage
  Artifact : spring-basic-security
  Description : Demo project for Spring Boot basic security
  Package : com.miage.security.api
  Dependencies : WEB , Devtools, Security
```
* 3- Generate the project
* 4- Unzip and import the project to Netbeans IDE

# Step 2 : Test the default spring boot security
* 1- open the main class
* 2- Enable the web security by adding the __@EnableWebSecurity__ annotation
```
  @SpringBootApplication
  @EnableWebSecurity
  public class SpringBasicSecurityApplication {

  	public static void main(String[] args) {
  		SpringApplication.run(SpringBasicSecurityApplication.class, args);
  	}

  }
```
* 3- Add the application controller

  - create a new java package : com.miage.security.api.controller
  - create java class  ApplicationController

  ```
  package com.miage.security.api.controller;
  public class ApplicationController {

  	public String greeting() {
  		return "spring security example";
  	}

  }
  ```
  - add __@RestController__  
  - define an entry point __(/rest/auth)__ for the controller : use the __@RequestMapping__ annotation
  - add __@GetMapping("/getMsg")__ to the greetting function


* 4- Add the spring security parameters as spring properties
  - open application.properties located at the resources folder
  - define a user name __spring.security.user.name=miage__
  - deine a new passord : __spring.security.user.password=password__


* 5- Run the project as java Application
* 6- Test the endpoint with postman :
  - send a get request http://localhost:8080/rest/auth/getMsg
* 7- Explain what happen ?
* 8- Add the authentication parmameters to the Authorization field
* 9- Re test again

## Step 3 : Spring security configuraiton based on URL
In this step we will test the spring security based on URL. Security mechanism will be applied only for a given route. To do this, we create a new Rest Controller __NoAuthController__ with entry point __('/noAuth/rest/')__, we configure our service by adding a simple gretting function eg.__sayHi()__ and then we specify a new security rule in (SpringSecurityConfig service) in order to secure only routes starting by __/rest/**__

* 1- create a new rest controller __NoAuthController__ with request mapping for __/noAuth/rest__
* 2- add a simple gretting function
```
  @GetMapping("/sayHi")
	public String sayHi() {
		return "hi";
	}
```
* 3- Create a new package : com.miage.security.api.config
* 4- Add a java class : SpringSecurityConfig

```
  package com.miage.security.api.config;

  import org.springframework.context.annotation.Bean;
  import org.springframework.context.annotation.Configuration;
  import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
  import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
  import org.springframework.security.config.annotation.web.builders.HttpSecurity;
  import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
  import org.springframework.security.config.http.SessionCreationPolicy;
  import org.springframework.security.crypto.password.NoOpPasswordEncoder;

  @SuppressWarnings("deprecation")
  @Configuration
  @EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
  public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {


    //users configuration
  	@Override
  	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
  		auth.inMemoryAuthentication().withUser("miage").password("password").roles("USER");
  	}



    //--------------------------------------------
    // Rule 1 : security for all API
    //--------------------------------------------
    @Override protected void configure(HttpSecurity http) throws Exception {    
      http.httpBasic().
        realmName("spring-app").
        and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).
        and().csrf().disable().
        authorizeRequests().anyRequest().fullyAuthenticated().and().
        httpBasic();
    }

    //--------------------------------------------
  	// Rule 2 : security based on URL
    //--------------------------------------------
      /*
      * add code herer
      */


    //--------------------------------------------
  	// Rule 3 : security based on ROLE
    //--------------------------------------------
      /*
      * add code herer
      */  


    @Bean
    public NoOpPasswordEncoder passwordEncoder() {
        return (NoOpPasswordEncoder) NoOpPasswordEncoder.getInstance();
    }
  }
```  

* 5- Comment the Rule 1 and define the Rule 2 as follow : only route start by __/rest/**__ must be secured
```
@Override
        protected void configure(HttpSecurity http) throws Exception {
            http.httpBasic().
                    realmName("spring-app").
                    and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).
                    and().csrf().disable().
                    authorizeRequests().antMatchers("/rest/**").fullyAuthenticated().and().httpBasic();
        }
```
* 6- test the security rules with postman
 - T1 : http://localhost:8080/noAuth/rest/sayHi with Autorisation = noAuth
 - T2 : http://localhost:8080/rest/auth/getMsg with Autorisation = { username : miage, password : password}

* 7- Explain what happened ?  

## Step 4 : Spring security configuration based on user role
in this step we will test spring security based on user role. we insert a new user name admin with role ADMIN, then we configure a new rule to allow only ADMIN user to reach '__/rest/**__'  

* 1- add a new user Admin, password admin and role Admin in the spring security config
* 2- comment Rule 2 and add Rule 3 as follow
```
@Override
	protected void configure(HttpSecurity http) throws Exception {
		 http.httpBasic().
                    realmName("spring-app").
                    and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).
                    and().csrf().disable().
                    authorizeRequests().antMatchers("/rest/**").hasAnyRole("ADMIN").anyRequest().fullyAuthenticated().and()
				.httpBasic();
	}
```
* 6- test the Rule 3 rules with postman
 - T1 : http://localhost:8080/rest/auth/getMsg with Autorisation = { username : miage, password : password}
 - T2 : http://localhost:8080/rest/auth/getMsg with Autorisation = { username : admin, password : admin}

* 7- Explain what happened ? What is code error ?


## Step 5 : Testing Role based Authorization with spring security

* 1- create a new package : com.miage.security.api.model
* 2- create a CustomerProfile model

```
package com.miage.security.api.model;

public class CustomerProfile {

    private String username;

    private String name;

    public CustomerProfile() {
        super();
    }

    public CustomerProfile(String username, String name) {
        this.name = name;
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}

```

* 3- create a CustomerControler
```

@RestController
public class CustomerController {

    @Secured("ROLE_USER")
    @GetMapping(value = "/user/hello")
    public String welcomeAppUser(@AuthenticationPrincipal User user) {
        return "Welcome User " + user.getUsername();
    }

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @GetMapping(value = "/user/hello4")
    public String welcomeAppUser4(@AuthenticationPrincipal User user) {
        return "Welcome User " + user.getUsername();
    }


    @PreAuthorize("hasRole('USER')")
    @GetMapping(value = "/user/hello1")
    public String welcomeAppUser1(@AuthenticationPrincipal User user) {
        return "Welcome User " + user.getUsername();
    }


    @PreAuthorize("hasRole('USER') AND hasRole('ADMIN')")
    @GetMapping(value = "/user/hello2")
    public String welcomeAppUser2(@AuthenticationPrincipal User user) {
        return "Welcome User " + user.getUsername();
    }

    @PreAuthorize("hasRole('USER') OR hasRole('ADMIN')")
    @GetMapping(value = "/user/hello3")
    public String welcomeAppUser3(@AuthenticationPrincipal User user) {
        return "Welcome User " + user.getUsername();
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping(value = "/user/hello5")
    public CustomerProfile welcomeAppUser5(@AuthenticationPrincipal User user) {
        return new CustomerProfile("Miage", "TOULOUSE");
    }


    @RolesAllowed("USER")
    @GetMapping(value = "/user/hello6")
    public String welcomeAppUser6(@AuthenticationPrincipal User user) {
        return "Welcome User " + user.getUsername();
    }


    @RolesAllowed({"USER", "ADMIN"})
    @GetMapping(value = "/user/hello7")
    public String welcomeAppUser7(@AuthenticationPrincipal User user) {
        return "Welcome User " + user.getUsername();
    }

    @GetMapping(value = "/rest/user")
    public CustomerProfile guestUser() {
        return new CustomerProfile("carthage","123");
    }
}

```
* 4- Explore the annotations and Role based authorisation usage
* 5- test with all end point with postman
